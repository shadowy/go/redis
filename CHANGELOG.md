<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-11-08
### Features
- auto open connection and add method for check connection status


<a name="v1.0.4"></a>
## [v1.0.4] - 2019-11-06
### Bug Fixes
- yaml bind


<a name="v1.0.3"></a>
## [v1.0.3] - 2019-11-06
### Features
- add remove functionality
- added the possibility to change protocol what produced by container (default: http)


<a name="v1.0.2"></a>
## [v1.0.2] - 2019-08-30
### Code Refactoring
- change default value for port fix: issue with address url


<a name="v1.0.1"></a>
## v1.0.1 - 2019-08-30
### Features
- KV functionality


[Unreleased]: https://gitlab.com/shadowy/go/redis/compare/v1.0.5...HEAD
[v1.0.5]: https://gitlab.com/shadowy/go/redis/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/redis/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/redis/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/redis/compare/v1.0.1...v1.0.2
