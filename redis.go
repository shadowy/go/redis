package redis

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/utils"
	"time"
)

// Redis - redis settings
type Redis struct {
	Host         string `yaml:"host"`         // Host
	Port         *int32 `yaml:"port"`         // Port default 6379
	Password     string `yaml:"password"`     // Password
	DB           int    `yaml:"db"`           // DB
	ValueTimeout *int   `yaml:"valueTimeout"` //ValueTimeout minutes default 60 minutes

	connectionString string
	client           *redis.Client
}

// Init - initialize properties
func (rd *Redis) Init() error {
	if rd.Port == nil {
		rd.Port = utils.PtrInt32(6379)
	}
	if rd.ValueTimeout == nil {
		rd.ValueTimeout = utils.PtrInt(int(time.Minute * 60))
	} else {
		rd.ValueTimeout = utils.PtrInt(int(time.Minute) * (*rd.ValueTimeout))
	}
	logrus.WithFields(rd.params(nil)).Info("Redis.Init")
	rd.buildConnectionString()
	return rd.Open()
}

/**
IsConnection - status connection
*/
func (rd *Redis) IsConnected() bool {
	if rd.client == nil {
		return false
	}
	return true
}

// Open - open connection to redis
func (rd *Redis) Open() error {
	logrus.WithFields(rd.params(nil)).Debug("Redis.Open")
	client := redis.NewClient(&redis.Options{
		Addr:     rd.connectionString,
		Password: rd.Password,
		DB:       rd.DB,
	})
	_, err := client.Ping().Result()
	if err != nil {
		logrus.WithFields(rd.params(nil)).WithError(err).Error("Redis.Open ping")
		return err
	}
	rd.client = client
	return nil
}

// Close - close connection to redis
func (rd *Redis) Close() error {
	logrus.WithFields(rd.params(nil)).Debug("Redis.Close")
	err := rd.client.Close()
	if err != nil {
		logrus.WithFields(rd.params(nil)).WithError(err).Error("Redis.Close")
	}
	rd.client = nil
	return err
}

// SetValue - store value into redis
func (rd *Redis) SetValue(key string, value interface{}) error {
	res := rd.client.Set(key, value, time.Duration(*rd.ValueTimeout))
	err := res.Err()
	if err != nil {
		logrus.WithFields(rd.params(map[string]interface{}{"field": key})).WithError(err).Error("Redis.SetValue")
	}
	return err
}

// SetValueWithTimeout - store value into redis with timeout
func (rd *Redis) SetValueWithTimeout(key string, value interface{}, timeout time.Duration) error {
	res := rd.client.Set(key, value, timeout)
	err := res.Err()
	if err != nil {
		logrus.WithFields(rd.params(map[string]interface{}{"field": key})).WithError(err).Error("Redis.SetValue")
	}
	return err
}

// GetValue - get value from redis
func (rd *Redis) GetValue(key string) (result interface{}, err error) {
	res := rd.client.Get(key)
	result, err = res.Result()
	if err != nil {
		logrus.WithFields(rd.params(map[string]interface{}{"field": key})).WithError(err).Error("Redis.GetValue")
		return
	}
	return
}

// Remove - remove value from redis
func (rd *Redis) Remove(key string) error {
	res := rd.client.Del(key)
	_, err := res.Result()
	if err != nil {
		logrus.WithField("key", key).WithError(err).Error("Redis.Remove")
		return err
	}
	return nil
}

func (rd *Redis) buildConnectionString() {
	rd.connectionString = fmt.Sprintf("%s:%d", rd.Host, *rd.Port)
}

func (rd *Redis) params(fields map[string]interface{}) logrus.Fields {
	res := logrus.Fields{
		"Host":         rd.Host,
		"Port":         rd.Port,
		"DB":           rd.DB,
		"ValueTimeout": time.Duration(*rd.ValueTimeout),
	}
	if fields != nil {
		for key, value := range fields {
			res[key] = value
		}
	}
	return res
}
